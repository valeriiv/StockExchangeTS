import Budgets from '../../types/Budgets';

const BUDGETS_KEY = 'budgets';

export const removeBudgetLS = (budgets: Budgets[], delKey: any): void => {
  budgets.splice(delKey, 1);

  try {
    localStorage.setItem(BUDGETS_KEY, JSON.stringify(budgets));
  } catch (e) {
    // eslint-disable-next-line
    console.error(e);
  }
};
