import Budgets from '../../types/Budgets';

const BUDGETS_KEY = 'budgets';

export const addToLS = (budget: Budgets[]): void => {
    try {
      localStorage.setItem(BUDGETS_KEY, JSON.stringify(budget));
    } catch (e) {
      // eslint-disable-next-line
      console.log(e);
    }
  };
  