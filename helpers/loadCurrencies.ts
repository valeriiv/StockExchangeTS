import axios, { AxiosResponse } from 'axios';

export async function get3Currencies(reqString: string, corsString: string = ''): Promise<any> {
  let request: AxiosResponse<any>;
  try {
    if (process.env.NODE_ENV === 'development') {
      request = await axios.get(`${corsString}${reqString}`);
    } else {
      request = await axios.get(`${reqString}`);
    }
    return request;
  } catch (err) {
    // eslint-disable-next-line
      console.log(err);
  }
}

export async function getHryvnya(reqString: string, corsString: string = ''): Promise<any> {
  let request: AxiosResponse<any>;
  try {
    if (process.env.NODE_ENV === 'development') {
      request = await axios.get(`${corsString}${reqString}`);
    } else {
      request = await axios.get(`${reqString}`);
    }
    return request;
  } catch (err) {
    // eslint-disable-next-line
      console.log(err);
  }
}
