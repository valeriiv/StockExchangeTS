import { Middleware } from '@nuxt/types';

const redirectTobudgets: Middleware = ({ route, redirect }) => {
  if (route.fullPath === '/') {
    return redirect('/budgets');
  }
};

export default redirectTobudgets;

