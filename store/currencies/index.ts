// import { GetterTree, ActionTree, MutationTree } from 'vuex';
// import { Decimal } from 'decimal.js';
// import Currencies from '~/../../types/Currencies';
// import { RootState } from '../index';

// //state
// export const state = () => {
//   return {
//     USD: new Decimal(0.0).toFixed(2),
//     EURO: new Decimal(0.0).toFixed(2),
//     GBP: new Decimal(0.0).toFixed(2),
//     RUB: new Decimal(0.0).toFixed(2),
//     HRV: new Decimal(0.0).toFixed(2)
//   };
// };

// export type CurrenciesState = ReturnType<typeof state>;

// //getters
// export const getters: GetterTree<CurrenciesState, RootState> = {
//     getUSD({ USD }) {
//     return USD;
//     },
//     getEURO({ EURO }) {
//     return EURO;
//     },
//     getgetGBP({ GBP }) {
//     return GBP;
//     },
//     getHRV({ HRV }) {
//     return HRV;
//     },
//     getRUB({ RUB }) {
//     return RUB;
//     },
// };

// //mutations
// export const mutations: MutationTree<CurrenciesState> = {};

// //actions
// export const actions: ActionTree<CurrenciesState, RootState> = {};