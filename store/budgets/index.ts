import Vue from 'vue';
import { Decimal } from 'decimal.js';
import Budgets from '~/types/Budgets';
import BudgetsState from '~/types/BudgetsState';
import { addToLS } from '~/helpers/localStorageWorkers/addToLocalStorage';
import { removeBudgetLS } from '~/helpers/localStorageWorkers/removeOneBudget';
import { receiveFromLS } from '~/helpers/localStorageWorkers/receiveFromLocalStorage';

import { GetterTree, ActionTree, MutationTree, Module } from 'vuex';

//state
const state = () => {
  return {
    budgets: [] as Budgets[],
    currentBudgetId: 0 as number,
  };
};

export type RootState = ReturnType<typeof state>;

//getters
const getters: GetterTree<RootState, RootState> = {
  getBudgets({ budgets }: BudgetsState) {
    return budgets;
  },
  getCurrentBudgetId({ currentBudgetId }: BudgetsState) {
    return currentBudgetId;
  },
};

//mutations
const mutations: MutationTree<RootState> = {
  CURRENT_BUDGET_ID(state: BudgetsState, payload: number) {
    state.currentBudgetId = payload;
  },
  DELETE_SELECTED_BUDGET(state: BudgetsState, payload: Budgets[]) {
    removeBudgetLS(state.budgets, payload);
  },
  SET_BUDGETS(state: BudgetsState, payload: { currency: string, limit: number, name: string }) {
    const { currency, limit, name } = payload;

    //@TODO change type
    const resultSingleBudget: any = {
      remBudget: new Decimal(0.0).toFixed(2),
      avgSumm: new Decimal(0.0).toFixed(2),
      minSumm: new Decimal(0.0).toFixed(2),
      maxSumm: new Decimal(0.0).toFixed(2),
      currency,
      limit: new Decimal(limit).toFixed(2),
      name,
      transactions: []
    };

  state.budgets.push(resultSingleBudget);

  addToLS(state.budgets);
  },
  RECEIVE_BUDGETS(state: BudgetsState) {
    receiveFromLS(state.budgets);
  },
  UPDATE_BUDGETS(state: BudgetsState, payload: { minSumm: Decimal, maxSumm: Decimal, avgSumm: Decimal, remBudget: Decimal }) {
    const index: number = Number(state.currentBudgetId);

    state.budgets[index].minSumm = payload.minSumm;
    state.budgets[index].maxSumm = payload.maxSumm;
    state.budgets[index].avgSumm = payload.avgSumm;
    state.budgets[index].remBudget = payload.remBudget;
  },
};

//actions
const actions: ActionTree<RootState, RootState> = {
  setBudgets({ commit }, payload: Budgets) {
    commit('SET_BUDGETS', payload);
  },
  receiveBudgets({ commit }, payload: Budgets) {
    commit('SET_BUDGETS', payload);
  },
};

//budgetsModule @TODO
export const budgets: Module<any, any> = {
  state,
  getters,
  mutations,
  actions
};