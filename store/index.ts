import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import { budgets } from './budgets';

 const store = () => new Vuex.Store({
  modules: {
    budgets
  }
});

export default store;