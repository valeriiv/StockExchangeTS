import { Decimal } from 'decimal.js';

export default interface ICurrencies {
    USD: Decimal,
    EURO: Decimal,
    GBP: Decimal,
    RUB: Decimal,
    HRV: Decimal
}