import Budgets from '~/types/Budgets';

export default interface IBudgetsState {
    budgets: Budgets[],
    currentBudgetId: number,
}