import { Decimal } from 'decimal.js';

export default interface IBudget {
    remBudget: Decimal,
    avgSumm: Decimal,
    minSumm: Decimal,
    maxSumm: Decimal,
    currency: any,
    limit: Decimal,
    name: string,
    transactions: [],
}