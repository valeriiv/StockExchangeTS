export default interface INewBudget {
    name: string,
    currency: string,
    limit: number
}